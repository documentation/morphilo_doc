
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Software Design &#8212; Morphilo  documentation</title>
    <link rel="stylesheet" href="../_static/alabaster.css" type="text/css" />
    <link rel="stylesheet" href="../_static/pygments.css" type="text/css" />
    <script type="text/javascript" src="../_static/documentation_options.js"></script>
    <script type="text/javascript" src="../_static/jquery.js"></script>
    <script type="text/javascript" src="../_static/underscore.js"></script>
    <script type="text/javascript" src="../_static/doctools.js"></script>
    <link rel="index" title="Index" href="../genindex.html" />
    <link rel="search" title="Search" href="../search.html" />
    <link rel="next" title="Data Model" href="datamodel.html" />
    <link rel="prev" title="Morphilo Project Documentation" href="../index.html" />
   
  <link rel="stylesheet" href="../_static/custom.css" type="text/css" />
  
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          <div class="body" role="main">
            
  <div class="section" id="software-design">
<h1>Software Design<a class="headerlink" href="#software-design" title="Permalink to this headline">¶</a></h1>
<div class="section" id="mvc-model">
<h2>MVC Model<a class="headerlink" href="#mvc-model" title="Permalink to this headline">¶</a></h2>
<p>A standard architecture for software has become a form of an
observer pattern called <em>Model-View-Controller (MVC)</em>-Model <a class="footnote-reference" href="#f3" id="id1">[3]</a>.
This is escpecially true for web-based applications that use
some form of a client-server architecture since these systems naturally divide
the browser view from the rest of the program logic and, if dynamically set up,
also from the data model usually running in an extra server as well.
As already implied, the MVC-pattern modularizes the program into three components: model, view, and
controller coupled <em>low</em> by interfaces. The view is concerned with
everything the actual user sees on the screen or uses to interact with the
machine. The controller is to recognize and process the events initiated by the
user and to update the view. Processing involves to communicate with the model.
This may involve to save or provide data from the data base.</p>
<p>From all that follows, MVC-models are especially supportive for reusing
existing software and promotes parallel development of its three components.
So the data model of an existing program can easily be changed without touching
the essentials of the program logic. The same is true for the code that handles
the view. Most of the time view and data model are the two components that need
to be changed so that the software appearance and presentation is adjusted to
the new user group as well as the different data is adjusted to the needs of the different
requirements of the new application. Nevertheless, if bugs or general changes in
the controller component have to be done, it usually does not affect
substantially the view and data model.</p>
<p>Another positive consequence of MVC-models is that several views (or even
models) could be used simultaneously. It means that the same data could be
presented differently on the user interface.</p>
</div>
<div class="section" id="morphilo-architecture">
<h2>Morphilo Architecture<a class="headerlink" href="#morphilo-architecture" title="Permalink to this headline">¶</a></h2>
<div class="figure" id="id4">
<img alt="../_images/architecture.png" src="../_images/architecture.png" />
<p class="caption"><span class="caption-text">Figure 1: Basic Architecture of a Take-&amp;-Share-Approach</span></p>
</div>
<p>The architecture of a possible <em>take-and-share</em> approach for language
resources is visualized in figure 1. Because the very gist
of the approach becomes clearer if describing a concrete example, the case of
annotating lexical derivatives of Middle English with the help of the Morphilo Tool
<a class="footnote-reference" href="#f1" id="id2">[1]</a> using a <a class="reference external" href="http://www.mycore.de">MyCoRe repository</a> is given as an illustration.
However, any other tool that helps with manual annotations and manages metadata of a corpus could be
substituted here instead. <a class="footnote-reference" href="#f2" id="id3">[2]</a></p>
<p>After inputting an untagged corpus or plain text, it is determined whether the
input material was annotated previously by a different user. This information is
usually provided by the metadata administered by the annotation tool; in the case at
hand, the <em>Morphilo</em> component. An alternative is a
simple table look-up for all occurring words in the datasets Corpus 1 through Corpus n. If contained
completely, the <em>yes</em>-branch is followed up further – otherwise <em>no</em>
succeeds. The difference between the two branches is subtle, yet crucial. On
both branches, the annotation tool (here <em>Morphilo</em>) is called, which, first,
sorts out all words that are not contained in the master database (here <em>MyCoRe</em> repository)
and, second, makes reasonable suggestions on an optimal annotation of
the items. The suggestions made to the user are based on simple string mapping of a saved list of prefixes and suffixes
whereas the remainder of the mapping is defined as the word root. The annotations are linked to the respective items (e.g. words) in the
text, but they are also persistently saved in an extra dataset, i.e. in figure 1 in one of the delineated Corpus 1
through n, together with all available metadata.</p>
<p>The difference between the two branches in figure 1 is that
in the <em>yes</em>-branch a comparison between the newly created dataset and
all of the previous datasets of this text is carried out while this is not
possible if a text was not annotated before. Within this
unit, all deviations and congruencies of the annotated items are marked and counted. The underlying
assumption is that with a growing number of comparable texts the
correct annotations approach a theoretic true value of a correct annotation
while errors level out provided that the sample size is large enough. How the
distribution of errors and correct annotations exactly looks like and if a
normal distribution can be assumed is still object of the ongoing research, but
independent of the concrete results, the component (called <em>compare
manual annotations</em> in figure 1) allows for specifying the
exact form of the sample population.
In fact, it is necessary at that point to define the form of the distribution,
sample size, and the rejection region. To be put it simple here, a uniform distribution in form of a threshold value
of e.g. 20 could be defined that specifies that a word has to be annotated equally by
20 different users before it enters the master database.</p>
<p>Continuing the information flow in figure 1 further, the threshold values or, if so defined,
the results of the statistical calculation of other distributions respectively are
delivered to the quality-control-component. Based on the statistics, the
respective items together with the metadata, frequencies, and, of course,
annotations are written to the master database. All information in the master
database is directly used for automated annotations. Thus it is directly matched
to the input texts or corpora respectively through the <em>Morphilo</em>-tool.
The annotation tool decides on the entries looked up in the master which items
are to be manually annotated.</p>
<p>The processes just described are all hidden from the user who has no possibility
to impact the set quality standards but by errors in the annotation process. The
user will only see the number of items of the input text he or she will process manually. The
annotator will also see an estimation of the workload beforehand. On this
number, a decision can be made if to start the annotation at all.  It will be
possible to interrupt the annotation work and save progress on the server. And
the user will have access to the annotations made in the respective dataset,
correct them or save them and resume later. It is important to note that the user will receive
the tagged document only after all items are fully annotated. No partially
tagged text can be output.</p>
</div>
<div class="section" id="repository-framework">
<h2>Repository Framework<a class="headerlink" href="#repository-framework" title="Permalink to this headline">¶</a></h2>
<div class="figure" id="id5">
<img alt="../_images/mycore_architecture-2.png" src="../_images/mycore_architecture-2.png" />
<p class="caption"><span class="caption-text">Figure 2: <a class="reference external" href="http://www.mycore.de">MyCoRe</a>-Architecture and Components</span></p>
</div>
<p>To specify the repository framework, the morphilo application logic will have to be implemented,
a data model specified, and the input, search and output mask programmed.</p>
<p>There are three directories which are
important for adjusting the MyCoRe framework to the needs of one’s own application.</p>
<p>These three directories
correspond essentially to the three components in the MVC model as explicated above. Roughly, they are also envisualized in figure 2 in the upper
right hand corner. More precisely, the view (<em>Layout</em> in figure 2) and the model layer
(<em>Datenmodell</em> in figure 2) can be done
completely via the <em>interface</em>, which is a directory with a predefined
structure and some standard files. For the configuration of the logic an extra directory is offered (<em>/src/main/java/custom/mycore/addons/</em>). Here all, java classes
extending the controller layer should be added.
Practically, all three MVC layers are placed in the
<em>src/main/</em>-directory of the application. In one of the subdirectories,
<em>datamodel/def,</em> the datamodel specifications are defined as xml files. It parallels the model
layer in the MVC pattern. How the data model was defined will be explained in the section Data Model.</p>
<p class="rubric">Notes</p>
<table class="docutils footnote" frame="void" id="f1" rules="none">
<colgroup><col class="label" /><col /></colgroup>
<tbody valign="top">
<tr><td class="label"><a class="fn-backref" href="#id2">[1]</a></td><td>Peukert, H. (2012): From Semi-Automatic to Automatic Affix Extraction in Middle English Corpora: Building a Sustainable Database for Analyzing Derivational Morphology over Time, Empirical Methods in Natural Language Processing, Wien, Scientific series of the ÖGAI, 413-23.</td></tr>
</tbody>
</table>
<table class="docutils footnote" frame="void" id="f2" rules="none">
<colgroup><col class="label" /><col /></colgroup>
<tbody valign="top">
<tr><td class="label"><a class="fn-backref" href="#id3">[2]</a></td><td>The source code of a possible implementation is available on <a class="reference external" href="https://github.com/amadeusgwin/morphilo">https://github.com/amadeusgwin/morphilo</a>. The software runs in test mode on <a class="reference external" href="https://www.morphilo.uni-hamburg.de/content/index.xml">https://www.morphilo.uni-hamburg.de/content/index.xml</a>.</td></tr>
</tbody>
</table>
<table class="docutils footnote" frame="void" id="f3" rules="none">
<colgroup><col class="label" /><col /></colgroup>
<tbody valign="top">
<tr><td class="label"><a class="fn-backref" href="#id1">[3]</a></td><td>Butz, Andreas; Antonio Krüger (2017): Mensch-Maschine-Interaktion, De Gruyter, 93ff.</td></tr>
</tbody>
</table>
</div>
</div>


          </div>
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
  <h3><a href="../index.html">Table Of Contents</a></h3>
  <ul>
<li><a class="reference internal" href="#">Software Design</a><ul>
<li><a class="reference internal" href="#mvc-model">MVC Model</a></li>
<li><a class="reference internal" href="#morphilo-architecture">Morphilo Architecture</a></li>
<li><a class="reference internal" href="#repository-framework">Repository Framework</a></li>
</ul>
</li>
</ul>
<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="../index.html">Documentation overview</a><ul>
      <li>Previous: <a href="../index.html" title="previous chapter">Morphilo Project Documentation</a></li>
      <li>Next: <a href="datamodel.html" title="next chapter">Data Model</a></li>
  </ul></li>
</ul>
</div>
  <div role="note" aria-label="source link">
    <h3>This Page</h3>
    <ul class="this-page-menu">
      <li><a href="../_sources/source/architecture.rst.txt"
            rel="nofollow">Show Source</a></li>
    </ul>
   </div>
<div id="searchbox" style="display: none" role="search">
  <h3>Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="../search.html" method="get">
      <input type="text" name="q" />
      <input type="submit" value="Go" />
      <input type="hidden" name="check_keywords" value="yes" />
      <input type="hidden" name="area" value="default" />
    </form>
    </div>
</div>
<script type="text/javascript">$('#searchbox').show(0);</script>
        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2018, Hagen Peukert.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 1.7.2</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.10</a>
      
      |
      <a href="../_sources/source/architecture.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>