.. Morphilo documentation master file, created by
   sphinx-quickstart on Fri Oct 12 14:26:54 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Morphilo Project Documentation
==============================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   source/architecture.rst
   source/datamodel.rst
   source/view.rst
   source/controller.rst
   

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
